FROM python:3

COPY requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt

ENV DATA_PATH=/data
VOLUME $DATA_PATH

COPY src /run
WORKDIR /run

ENTRYPOINT ["python3", "main.py"]
CMD ["--destination", "${DATA_PATH}"]
