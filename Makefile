.PHONY: fetch*

IMAGE_NAME ?= slurp
TARGET_DIR ?= `pwd`
GROUP_NAME ?= DEFINE_ME
API_TOKEN ?= DEFINE_ME
API_URL ?= https://gitlab.com/api/v4


python_install:
	pip3 install -r ./requirements.txt

docker_build:
	docker build -t $(IMAGE_NAME) .

fetch_python: python_install
	python3 src/cli.py \
	   --api-url="$(API_URL)" \
	   --api-token="$(API_TOKEN)" \
	   --clone-method="$(CLONE_METHOD)" \
	   --group-name="$(GROUP_NAME)" \
	   --destination="$(TARGET_DIR)"

fetch_docker_ssh: docker_build
	docker run --rm -it \
	  -v "$(TARGET_DIR):/data" \
	  -v /etc/passwd:/etc/passwd:ro \
	  -u "`id -u`:`id -g`" \
	  -v $$HOME/.ssh:$$HOME/.ssh \
	   "$(IMAGE_NAME)" \
	   --api-url="$(API_URL)" \
	   --api-token="$(API_TOKEN)" \
	   --clone-method=SSH \
	   --group-name="$(GROUP_NAME)" \
	   --destination=/data

fetch_docker_https: docker_build
	docker run --rm -it \
	  -v "$(TARGET_DIR):/data" \
	  -v /etc/passwd:/etc/passwd:ro \
	  -u "`id -u`:`id -g`" \
	   "$(IMAGE_NAME)" \
	   --api-url="$(API_URL)" \
	   --api-token="$(API_TOKEN)" \
	   --clone-method=HTTPs \
	   --group-name="$(GROUP_NAME)" \
	   --destination=/data
