# GitLab slurper

Clone all repositories of a GitLab group or subgroup, easily.

## Usage
Using python3:
```shell script
make fetch_python \
     -e TARGET_DIR="/tmp/wherever" \
     -e CLONE_METHOD="<SSH|HTTPs>" \
     -e GITLAB_TOKEN="<your private GitLab token>" \
     -e GROUP_NAME="path/of/your/group/or/subgroup"
```

Using docker:
```shell script
make fetch_docker_ssh \
     -e TARGET_DIR="/tmp/wherever" \
     -e GITLAB_TOKEN="<your private GitLab token>" \
     -e GROUP_NAME="path/of/your/group/or/subgroup"
```

Using docker (git over HTTPs):
```shell script
make fetch_docker_https \
     -e TARGET_DIR="/tmp/wherever" \
     -e GITLAB_TOKEN="<your private GitLab token>" \
     -e GROUP_NAME="path/of/your/group/or/subgroup"
```

Available `-e` parameters here are:
* `GITLAB_TOKEN`: private GitLab token to use when querying APIs (and when cloning via HTTPs)
* `GROUP_NAME`: group or subgroup to fetch
* `CLONE_METHOD`: clone via `SSH` or `HTTPs`
* `TARGET_DIR`: root directory of the cloned repositories (defaults to the current directory)
* `GITLAB_URL`: the base URL of your GitLab server (defaults to https://gitlab.com)

## Known defects
* SSH method uses the current user's default SSH key (no way to use another one, no passphrase handling...)
* HTTPs method writes the token, unencrypted, on the local filesystem
  (in the `.git/config` file of each repository)

## TEST 

**gitlab**
export API_URL=api.github.com
export API_TOKEN=afc79b4c2d112c06b8c4d5b0bad1dcaa50c55cdf

export GROUP_NAME=pigroupe && make fetch_docker_https

**github**
export API_URL=gitlab.com/api/v4
export API_URL=X2MzSE3XM1uE3s2j-RxH

export GROUP_NAME=pi-backup && make fetch_docker_https


## TODO
* HAve to work with github (api list https://api.github.com/users/pigroupe/repos)