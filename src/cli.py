import argparse
import os


from gitlab_api import GitLabApi
from gitlab_clone import slurp

gitlab_com = "https://gitlab.com"

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=self.coordinate)

    parser.add_argument("--gitlab-url",
                        default=gitlab_com,
                        help="the base URL of your GitLab server")
    parser.add_argument("--token",
                        help="the private token to use when accessing GitLab APIs (and when using HTTPs clone method)")
    parser.add_argument("--group-name",
                        help="the name of the GitLab group to fetch")
    parser.add_argument("--api-version",
                        default="v4",
                        help="the API version")
    parser.add_argument("--depth",
                        default=10,
                        help="the depth of sub-groups to fetch")
    parser.add_argument("--destination",
                        default=os.curdir,
                        help="the target root directory for repositories")
    parser.add_argument("--clone-method",
                        default="SSH",
                        help="the clone method (SSH/HTTPs) (defaults to SSH)")
    args = parser.parse_args()

    gitlab_api = GitLabApi(gitlab_url=args.gitlab_url,
                           clone_method=args.clone_method,
                           token=args.token,
                           api_version=args.api_version)

    slurp(gitlab_api=gitlab_api,
          group_name=args.group_name,
          depth=args.depth,
          destination=args.destination)

