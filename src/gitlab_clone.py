import os

from gitlab_api import GitLabApi


def slurp(group_name: str, gitlab_api: GitLabApi, depth=10, destination=os.curdir):
    """
    Discover and clone GitLab projects in a given group hierarchy.
    """

    root_group_id = gitlab_api.resolve_group_id(group_name)
    slurp_group(gitlab_api=gitlab_api, group_id=root_group_id, depth=depth, destination=destination)


def slurp_group(gitlab_api: GitLabApi, group_id: int, depth=1, destination=os.curdir):
    """
    Clone all projects in the given group, then recurse on subgroups (based on required depth).
    """

    group = gitlab_api.get_group(group_id)

    for project in group["projects"]:
        gitlab_api.clone(project, destination)

    if depth > 1:
        subgroups = gitlab_api.get_subgroups(group_id)
        for subgroup in subgroups:
            slurp_group(gitlab_api=gitlab_api, group_id=subgroup["id"], depth=depth - 1, destination=destination)
