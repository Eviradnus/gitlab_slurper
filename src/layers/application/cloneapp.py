from typing import Union
from ddd_domain_driven_design.application.observable.observable import Observable

from src.layers.application.cqrs.command.clonecmd import CloneCommand
from src.layers.domain.observer.github_api import GithubApi
from src.layers.domain.observer.gitlab_api import GitLabApi


class CloneApp(object):

    def __init__(self, CloneCommand: Union[CloneCommand, None], logger=None):
        self.command = CloneCommand
        self.logger = logger

    def run(self):
        """
            running discovery application

            :return:
        """
        observable = Observable()
        observable.attach(GithubApi(self.logger))
        observable.attach(GitLabApi(self.logger))
        observable.process(None)


