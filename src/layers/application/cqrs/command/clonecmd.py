"""Define Discovery DTO object"""
import re
from ddd_domain_driven_design.application.dto.generalisation.dto import DTO


class CloneCommand(DTO):
    """Discovery DTO object"""

    api_url = str, {"immutable": True}
    api_token = str, {"immutable": True}
    group_name = str, {"immutable": True}
    depth = str, {"immutable": True}
    destination = str, {"immutable": True}
    clone_method = str, {"immutable": True}
    log_level = str, {"immutable": True}

