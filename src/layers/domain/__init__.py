from ddd_domain_driven_design.application.observable.generalisation.interface.observer import ObserverInterface
from src.layers.domain.slurp.generalisation.interface.slurp import SlurpInterface

import importlib


class Factory:
    _instances = {}

    @staticmethod
    def slurp(api: ObserverInterface, api_type) -> SlurpInterface:
        """Return adapter for the desired slurp"""
        key_class = 'slurp'
        if key_class not in Factory._instances:

            slurp_class = getattr(
                importlib.import_module('src.layers.domain.slurp.%s' % api_type),
                api_type.capitalize()
            )
            Factory._instances[key_class] = slurp_class(api)

        return Factory._instances[key_class]

