from zope.interface import implementer
from ddd_domain_driven_design.application.observable.generalisation.interface.observer import ObserverInterface
from ddd_domain_driven_design.application.observable.generalisation.interface.observable import ObservableInterface

from src.layers.application.cqrs.command.clonecmd import CloneCommand
from ... import Factory
from src.layers.domain.observer.generalisation.interface.api import ApiInterface


@implementer(ObserverInterface)
@implementer(ApiInterface)
class BaseApi:
    """
    Base Handles access to all APIs.
    """

    def __init__(self, logger=None):
        self.logger = logger

    def update(self, subject: ObservableInterface):
        """
            Sync register services with effective running services

            :return:
        """
        self.base_api_url = self.set_base_api_url(subject.command)
        self.token = subject.command.api_token
        self.clone_method = subject.command.clone_method
        self.group_name = subject.command.group_name
        self.depth = subject.command.depth
        self.destination = subject.command.destination

        Factory.slurp(self, api_type=self.api_type).run()

    def set_base_api_url(self, command: CloneCommand):
        self.base_api_url = command.api_url
