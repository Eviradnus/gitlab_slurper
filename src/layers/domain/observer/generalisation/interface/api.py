"""Api Interface"""
from zope.interface import Interface
from src.layers.application.cqrs.command.clonecmd import CloneCommand

import dataclasses
import os


@dataclass
class ApiInterface(Interface):
    """
    Class for keeping track of an item in inventory.
    """
    base_api_url: str = ''
    token: str = ''
    clone_method: str = ''
    group_name: str = ''
    depth: int = 10
    destination: str = os.curdir
    api_type: str = ''

    def set_base_api_url(command: CloneCommand):
        """
        Set the base api url
        """

    def resolve_group_id(self) -> int:
        """
        Identify a group by name.
        """

    def get_group(self, group_id: int):
        """
        Get a group by ID.
        """

    def get_subgroups(self, group_id: int):
        """
        Get all subgroups from ID group parent.
        """
