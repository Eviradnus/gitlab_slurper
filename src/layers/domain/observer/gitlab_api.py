from src.layers.domain.observer.generalisation.baseapi import BaseApi

import json
import os

import requests
import git
import time
from git import Repo


class GitLabApi(BaseApi):
    """
    Handles access to the Github API.
    """

    '''Class for keeping track of an item in inventory.'''
    base_api_url: str = 'gitlab_com/api/v4'
    api_type: str = 'gitlab'

    def resolve_group_id(self) -> int:
        """
        Identify a group by name.
        """

        search_group_url = "%s/groups" % self.base_api_url
        print("Searching for group:", search_group_url)
        results = self.get(search_group_url, params={"scope": "groups",
                                                     "search": (self.group_name.rsplit("/")[-1])})

        wanted_results = list(e for e in results if e["full_path"].lower() == self.group_name.lower())
        if len(wanted_results) != 1:
            print("Could not pinpoint a unique valid group (found %s amongst %s search results)" % (
                len(wanted_results), len(results)))
            print(results)
            exit(2)

        result = results[0]["id"]
        print("Resolved id:", result)
        return result

    def get(self, url, headers={}, params={}):
        headers["Private-Token"] = self.token
        r = requests.get(url, headers=headers, params=params)
        if not r.ok:
            print("error %s: %s" % (r.status_code, r.text))
            exit(1)
        return json.loads(r.text)

    def get_group(self, group_id: int):
        """
        Get a group by ID.
        """

        get_group_url = "%s/groups/%s" % (self.base_api_url, group_id)
        print("Getting group metadata:", get_group_url)
        return self.get(get_group_url)

    def get_subgroups(self, group_id: int):
        """
        Get all subgroups from ID group parent.
        """

        get_subgroups_url = "%s/groups/%s/subgroups" % (self.base_api_url, group_id)
        print("Getting subgroups:", get_subgroups_url)
        return self.get(get_subgroups_url)

    def clone(self, project, destination=os.curdir):
        if self.clone_method.lower() == "https":
            self.clone_https(project, destination)
        else:
            self.clone_ssh(project, destination)

    @staticmethod
    def clone_ssh(project, destination=os.curdir):
        """
        Clone the given project into the target directory via SSH.
        """
        url = project["ssh_url_to_repo"]
        target_dir = os.path.join(destination, project["path_with_namespace"])
        print("Cloning %s into %s" % (url, target_dir))
        try:
            Repo.clone_from(url, target_dir)
        except git.GitCommandError as exc:
            time.sleep(1)  # Wait one second before retry

    def clone_https(self, project, destination=os.curdir):
        """
        Clone the given project into the target directory via HTTPs.
        """
        url = project["http_url_to_repo"]
        target_dir = os.path.join(destination, project["path_with_namespace"])
        print("Cloning %s into %s" % (url, target_dir))
        try:
            Repo.clone_from(url.replace("://", "://token:%s@" % self.token), target_dir)
        except git.GitCommandError as exc:
            time.sleep(1)  # Wait one second before retry
