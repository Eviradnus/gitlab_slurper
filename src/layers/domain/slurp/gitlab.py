from zope.interface import implementer

from src.layers.domain.observer.generalisation.interface.api import ApiInterface
from src.layers.domain.slurp.generalisation.interface.slurp import SlurpInterface

import os


@implementer(SlurpInterface)
class Gitlab:

    def __init__(self, api: ApiInterface):
        self.api = api

    def run(self):
        """
        Discover and clone GitLab projects in a given group hierarchy.
        """

        root_group_id = self.api.resolve_group_id()

        self.slurp_group(group_id=root_group_id)


    def slurp_group(self, group_id: int):
        """
        Clone all projects in the given group, then recurse on subgroups (based on required depth).
        """
        group = self.api.get_group(group_id)

        for project in group["projects"]:
            self.api.clone(project, self.api.destination)

        if self.api.depth > 1:
            subgroups = self.api.get_subgroups(group_id)
            for subgroup in subgroups:
                self.slurp_group(group_id=subgroup["id"])
