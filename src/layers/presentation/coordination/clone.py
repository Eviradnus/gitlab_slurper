import os
from ddd_domain_driven_design.presentation.adapter.commandadapter import CommandAdapter

from src.layers.presentation.request.command.clonerequest import CloneRequest
from src.layers.application.cloneapp import CloneApp
from src.layers.application.cqrs.command.clonecmd import CloneCommand

DEFAULT_LOG_LEVEL = 'info'
gitlab_com = "https://gitlab.com"


class Clone:
    name = "clone"

    def options(self, subparsers):
        parser = subparsers.ArgumentParser()
        parser.set_defaults(func=self.coordinate)

        parser.add_argument("--api-url",
                            help="the base URL of your GitLab server")
        parser.add_argument("--api-token",
                            help="the private token to use when accessing GitLab APIs (and when using HTTPs clone method)")
        parser.add_argument("--group-name",
                            help="the name of the GitLab group to fetch")
        parser.add_argument("--depth",
                            help="the depth of sub-groups to fetch")
        parser.add_argument("--destination",
                            help="the target root directory for repositories")
        parser.add_argument("--clone-method",
                            default="SSH",
                            help="the clone method (SSH/HTTPs) (defaults to SSH)")

    @staticmethod
    def coordinate(args, logger=None):
        """
        Coordinate the orchestration of services in the backend
        :param args:
        :param logger:
        :return:
        """
        adapter = CommandAdapter(CloneCommand)
        command = adapter.create_command_from_request(CloneRequest(args))

        application = CloneApp(command, logger)

        application.run()
