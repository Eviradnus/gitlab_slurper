from zope.interface import implementer
from ddd_domain_driven_design.presentation.request.generalisation.interface.request import RequestInterface


@implementer(RequestInterface)
class CloneRequest(object):
    """Adapter to create Command"""

    def __init__(self, args):
        self.args = args

    def get_request_parameters(self):
        """Create json string data from args"""

        return '{"api_url": "%s", "api_token": "%s", "group_name": "%s", "depth": "%s", "destination": "%s", "clone_method": "%s", "log_level": "%s"}' % (self.args.api_url,
                                                                                            self.args.api_token,
                                                                                            self.args.group_name,
                                                                                            self.args.depth,
                                                                                            self.args.destination,
                                                                                            self.args.clone_method,
                                                                                            self.args.log_level.upper())
